﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PerfumeManagementSystem.Entities
{
    public class Perfume
    {
        [Key]
        public Guid PerfumeId { set; get; }
        [Required]
        public string PerfumeName { set; get; }
        [Required]
        public int Quantity { set; get; }
    }
}

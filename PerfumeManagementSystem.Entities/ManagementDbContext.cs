﻿using System;
using Microsoft.EntityFrameworkCore;

namespace PerfumeManagementSystem.Entities
{
    public class ManagementDbContext : DbContext
    {
        public ManagementDbContext(DbContextOptions<ManagementDbContext> options) : base(options)
        {
        }

        public DbSet<Perfume> Perfumes { set; get; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Perfume>().ToTable("perfume");
        }
    }
}

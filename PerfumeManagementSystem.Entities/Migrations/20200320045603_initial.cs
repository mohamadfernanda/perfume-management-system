﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PerfumeManagementSystem.Entities.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "perfume",
                columns: table => new
                {
                    PerfumeId = table.Column<Guid>(nullable: false),
                    PerfumeName = table.Column<string>(nullable: false),
                    Quantity = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_perfume", x => x.PerfumeId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "perfume");
        }
    }
}

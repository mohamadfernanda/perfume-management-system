﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace PerfumeManagementSystem.Entities
{
    public class ManagementDbContextFactory : IDesignTimeDbContextFactory<ManagementDbContext>
    {
        public ManagementDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<ManagementDbContext>();
            builder.UseSqlite("Data Source=reference.db");
            return new ManagementDbContext(builder.Options);
        }
    }
}

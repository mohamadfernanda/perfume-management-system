﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PerfumeManagementSystem.Models;
using PerfumeManagementSystem.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PerfumeManagementSystem.API
{
    [Route("api/v1/perfume")]
    [ApiController]
    public class PerfumeApiController : ControllerBase
    {
        private readonly PerfumeService ServiceMan;

        public PerfumeApiController(PerfumeService perfumeService)
        {
            this.ServiceMan = perfumeService;
        }

        [HttpGet("all-perfume", Name = "getAllPerfume")]
        public async Task<ActionResult<List<PerfumeModel>>> GetAllPerfumeAsync()
        {
            var perfumes = await ServiceMan.GetAllPerfumesAsync();
            return Ok(perfumes);
        }

        [HttpPost("insert", Name = "insertPerfume")]
        public async Task<ActionResult<ResponseModel>> InsertNewPerfumeAsync([FromBody]PerfumeModel value)
        {
            var isSuccess = await ServiceMan.InsertPerfume(value);
            return Ok(new ResponseModel
            {
                ResponseMessage = "Success inserting new data."
            });
        }

        [HttpPut("update", Name = "updatePerfume")]
        public async Task<ActionResult<ResponseModel>> UpdatePerfumeAsync([FromBody] PerfumeModel value)
        {
            var isSuccess = await ServiceMan.UpdatePerfumeAsync(value);

            if (isSuccess == false)
            {
                return BadRequest(new ResponseModel
                {
                    ResponseMessage = "ID Not Found!!!"
                });
            }

            return Ok(new ResponseModel
            {
                ResponseMessage = $"Success update data {value.PerfumeName}"
            });
        }

        [HttpDelete("delete", Name = "deletePerfume")]
        public async Task<ActionResult<ResponseModel>> DeletePerfumeAsync([FromBody] PerfumeModel value)
        {
            var isSuccess = await ServiceMan.DeletePerfumeAsync(value.PerfumeId);

            if (isSuccess == false)
            {
                return BadRequest(new ResponseModel
                {
                    ResponseMessage = "Id not found"
                });
            }

            return Ok(new ResponseModel
            {
                ResponseMessage = $"Success delete perfume {value.PerfumeName}"
            });
        }

        [HttpGet("specific-perfume", Name = "getSpesificPerfume")]
        public async Task<ActionResult<PerfumeModel>> GetSpecificPerfumeAsync(Guid? perfumeId)
        {
            if (perfumeId.HasValue == false)
            {
                return BadRequest(null);
            }

            var perfume = await ServiceMan.GetSpesificPerfumeAsync(perfumeId.Value);
            if (perfume == null)
            {
                return BadRequest(null);
            }
            return Ok(perfume);
        }

        [HttpDelete("delete-2/{perfumeId}", Name = "deletePerfume2")] //API With Parameter
        public async Task<ActionResult<ResponseModel>> DeletePerfume2Async(Guid perfumeId)
        {
            var isSuccess = await ServiceMan.DeletePerfumeAsync(perfumeId);

            if (isSuccess == false)
            {
                return BadRequest(new ResponseModel
                {
                    ResponseMessage = "Perfume Id not found"
                });
            }

            return Ok(new ResponseModel
            {
                ResponseMessage = "Success delete perfume"
            });
        }

        [HttpDelete("delete-query")]
        public async Task<IActionResult> DeletePerfumeQueryAsync([FromQuery] Guid perfumeId)
        {
            var isSuccess = await ServiceMan.DeletePerfumeAsync(perfumeId);

            if (isSuccess == false)
            {
                return BadRequest("Id not found!");
            }

            return Ok(isSuccess);
        }


        [HttpGet("filter-data")]
        public async Task<ActionResult<List<PerfumeModel>>> GetFilterDataAsync([FromQuery] int pageIndex, int itemPerPage, string filterByName)
        {
            var data = await ServiceMan.GetAsync(pageIndex, itemPerPage, filterByName);

            return Ok(data);
        }

        [HttpGet("total-data")]
        public ActionResult<int> GetTotalData()
        {
            var totalData = ServiceMan.GetTotalData();

            return Ok(totalData);
        }
    }
}

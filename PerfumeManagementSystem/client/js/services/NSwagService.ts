﻿/* tslint:disable */
/* eslint-disable */
//----------------------
// <auto-generated>
//     Generated using the NSwag toolchain v13.2.5.0 (NJsonSchema v10.1.7.0 (Newtonsoft.Json v11.0.0.0)) (http://NSwag.org)
// </auto-generated>
//----------------------
// ReSharper disable InconsistentNaming

export class PerfumeService {
    private http: { fetch(url: RequestInfo, init?: RequestInit): Promise<Response> };
    private baseUrl: string;
    protected jsonParseReviver: ((key: string, value: any) => any) | undefined = undefined;

    constructor(baseUrl?: string, http?: { fetch(url: RequestInfo, init?: RequestInit): Promise<Response> }) {
        this.http = http ? http : <any>window;
        this.baseUrl = baseUrl ? baseUrl : "";
    }

    /**
     * @return Success
     */
    allPerfume(): Promise<void> {
        let url_ = this.baseUrl + "/api/v1/perfume/all-perfume";
        url_ = url_.replace(/[?&]$/, "");

        let options_ = <RequestInit>{
            method: "GET",
            headers: {
            }
        };

        return this.http.fetch(url_, options_).then((_response: Response) => {
            return this.processAllPerfume(_response);
        });
    }

    protected processAllPerfume(response: Response): Promise<void> {
        const status = response.status;
        let _headers: any = {}; if (response.headers && response.headers.forEach) { response.headers.forEach((v: any, k: any) => _headers[k] = v); };
        if (status === 200) {
            return response.text().then((_responseText) => {
                return;
            });
        } else if (status !== 200 && status !== 204) {
            return response.text().then((_responseText) => {
                return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            });
        }
        return Promise.resolve<void>(<any>null);
    }

    /**
     * @param body (optional) 
     * @return Success
     */
    insertPerfume(body: PerfumeModel | undefined): Promise<ResponseModel> {
        let url_ = this.baseUrl + "/api/v1/perfume/insert";
        url_ = url_.replace(/[?&]$/, "");

        const content_ = JSON.stringify(body);

        let options_ = <RequestInit>{
            body: content_,
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Accept": "text/plain"
            }
        };

        return this.http.fetch(url_, options_).then((_response: Response) => {
            return this.processInsertPerfume(_response);
        });
    }

    protected processInsertPerfume(response: Response): Promise<ResponseModel> {
        const status = response.status;
        let _headers: any = {}; if (response.headers && response.headers.forEach) { response.headers.forEach((v: any, k: any) => _headers[k] = v); };
        if (status === 200) {
            return response.text().then((_responseText) => {
                let result200: any = null;
                result200 = _responseText === "" ? null : <ResponseModel>JSON.parse(_responseText, this.jsonParseReviver);
                return result200;
            });
        } else if (status !== 200 && status !== 204) {
            return response.text().then((_responseText) => {
                return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            });
        }
        return Promise.resolve<ResponseModel>(<any>null);
    }

    /**
     * @param body (optional) 
     * @return Success
     */
    updatePerfume(body: PerfumeModel | undefined): Promise<ResponseModel> {
        let url_ = this.baseUrl + "/api/v1/perfume/update";
        url_ = url_.replace(/[?&]$/, "");

        const content_ = JSON.stringify(body);

        let options_ = <RequestInit>{
            body: content_,
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                "Accept": "text/plain"
            }
        };

        return this.http.fetch(url_, options_).then((_response: Response) => {
            return this.processUpdatePerfume(_response);
        });
    }

    protected processUpdatePerfume(response: Response): Promise<ResponseModel> {
        const status = response.status;
        let _headers: any = {}; if (response.headers && response.headers.forEach) { response.headers.forEach((v: any, k: any) => _headers[k] = v); };
        if (status === 200) {
            return response.text().then((_responseText) => {
                let result200: any = null;
                result200 = _responseText === "" ? null : <ResponseModel>JSON.parse(_responseText, this.jsonParseReviver);
                return result200;
            });
        } else if (status !== 200 && status !== 204) {
            return response.text().then((_responseText) => {
                return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            });
        }
        return Promise.resolve<ResponseModel>(<any>null);
    }

    /**
     * @param body (optional) 
     * @return Success
     */
    deletePerfume(body: PerfumeModel | undefined): Promise<ResponseModel> {
        let url_ = this.baseUrl + "/api/v1/perfume/delete";
        url_ = url_.replace(/[?&]$/, "");

        const content_ = JSON.stringify(body);

        let options_ = <RequestInit>{
            body: content_,
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
                "Accept": "text/plain"
            }
        };

        return this.http.fetch(url_, options_).then((_response: Response) => {
            return this.processDeletePerfume(_response);
        });
    }

    protected processDeletePerfume(response: Response): Promise<ResponseModel> {
        const status = response.status;
        let _headers: any = {}; if (response.headers && response.headers.forEach) { response.headers.forEach((v: any, k: any) => _headers[k] = v); };
        if (status === 200) {
            return response.text().then((_responseText) => {
                let result200: any = null;
                result200 = _responseText === "" ? null : <ResponseModel>JSON.parse(_responseText, this.jsonParseReviver);
                return result200;
            });
        } else if (status !== 200 && status !== 204) {
            return response.text().then((_responseText) => {
                return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            });
        }
        return Promise.resolve<ResponseModel>(<any>null);
    }

    /**
     * @param employeeId (optional) 
     * @return Success
     */
    getSpesificPerfume(perfumeId: string | undefined): Promise<PerfumeModel> {
        let url_ = this.baseUrl + "/api/v1/perfume/specific-perfume?";
        if (perfumeId === null)
            throw new Error("The parameter 'perfumeId' cannot be null.");
        else if (perfumeId !== undefined)
            url_ += "perfumeId=" + encodeURIComponent("" + perfumeId) + "&";
        url_ = url_.replace(/[?&]$/, "");

        let options_ = <RequestInit>{
            method: "GET",
            headers: {
                "Accept": "text/plain"
            }
        };

        return this.http.fetch(url_, options_).then((_response: Response) => {
            return this.processGetSpesificPerfume(_response);
        });
    }

    protected processGetSpesificPerfume(response: Response): Promise<PerfumeModel> {
        const status = response.status;
        let _headers: any = {}; if (response.headers && response.headers.forEach) { response.headers.forEach((v: any, k: any) => _headers[k] = v); };
        if (status === 200) {
            return response.text().then((_responseText) => {
                let result200: any = null;
                result200 = _responseText === "" ? null : <PerfumeModel>JSON.parse(_responseText, this.jsonParseReviver);
                return result200;
            });
        } else if (status !== 200 && status !== 204) {
            return response.text().then((_responseText) => {
                return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            });
        }
        return Promise.resolve<PerfumeModel>(<any>null);
    }

    /**
     * @return Success
     */
    deletePerfume2(perfumeId: string): Promise<ResponseModel> {
        let url_ = this.baseUrl + "/api/v1/perfume/delete-2/{perfumeId}";
        if (perfumeId === undefined || perfumeId === null)
            throw new Error("The parameter 'perfumeId' must be defined.");
        url_ = url_.replace("{perfumeId}", encodeURIComponent("" + perfumeId));
        url_ = url_.replace(/[?&]$/, "");

        let options_ = <RequestInit>{
            method: "DELETE",
            headers: {
                "Accept": "text/plain"
            }
        };

        return this.http.fetch(url_, options_).then((_response: Response) => {
            return this.processDeletePerfume2(_response);
        });
    }

    protected processDeletePerfume2(response: Response): Promise<ResponseModel> {
        const status = response.status;
        let _headers: any = {}; if (response.headers && response.headers.forEach) { response.headers.forEach((v: any, k: any) => _headers[k] = v); };
        if (status === 200) {
            return response.text().then((_responseText) => {
                let result200: any = null;
                result200 = _responseText === "" ? null : <ResponseModel>JSON.parse(_responseText, this.jsonParseReviver);
                return result200;
            });
        } else if (status !== 200 && status !== 204) {
            return response.text().then((_responseText) => {
                return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            });
        }
        return Promise.resolve<ResponseModel>(<any>null);
    }

    /**
     * @param employeeId (optional) 
     * @return Success
     */
    deleteQuery(perfumeId: string | undefined): Promise<void> {
        let url_ = this.baseUrl + "/api/v1/perfume/delete-query?";
        if (perfumeId === null)
            throw new Error("The parameter 'perfumeId' cannot be null.");
        else if (perfumeId !== undefined)
            url_ += "perfumeId=" + encodeURIComponent("" + perfumeId) + "&";
        url_ = url_.replace(/[?&]$/, "");

        let options_ = <RequestInit>{
            method: "DELETE",
            headers: {
            }
        };

        return this.http.fetch(url_, options_).then((_response: Response) => {
            return this.processDeleteQuery(_response);
        });
    }

    protected processDeleteQuery(response: Response): Promise<void> {
        const status = response.status;
        let _headers: any = {}; if (response.headers && response.headers.forEach) { response.headers.forEach((v: any, k: any) => _headers[k] = v); };
        if (status === 200) {
            return response.text().then((_responseText) => {
                return;
            });
        } else if (status !== 200 && status !== 204) {
            return response.text().then((_responseText) => {
                return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            });
        }
        return Promise.resolve<void>(<any>null);
    }

    /**
     * @return Success
     */
    allData2(): Promise<void> {
        let url_ = this.baseUrl + "/api/v1/perfume/all-data2";
        url_ = url_.replace(/[?&]$/, "");

        let options_ = <RequestInit>{
            method: "GET",
            headers: {
            }
        };

        return this.http.fetch(url_, options_).then((_response: Response) => {
            return this.processAllData2(_response);
        });
    }

    protected processAllData2(response: Response): Promise<void> {
        const status = response.status;
        let _headers: any = {}; if (response.headers && response.headers.forEach) { response.headers.forEach((v: any, k: any) => _headers[k] = v); };
        if (status === 200) {
            return response.text().then((_responseText) => {
                return;
            });
        } else if (status !== 200 && status !== 204) {
            return response.text().then((_responseText) => {
                return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            });
        }
        return Promise.resolve<void>(<any>null);
    }

    /**
     * @param pageIndex (optional) 
     * @param itemPerPage (optional) 
     * @param filterByName (optional) 
     * @return Success
     */
    filterData(pageIndex: number | undefined, itemPerPage: number | undefined, filterByName: string | undefined): Promise<PerfumeModel[]> {
        let url_ = this.baseUrl + "/api/v1/perfume/filter-data?";
        if (pageIndex === null)
            throw new Error("The parameter 'pageIndex' cannot be null.");
        else if (pageIndex !== undefined)
            url_ += "pageIndex=" + encodeURIComponent("" + pageIndex) + "&";
        if (itemPerPage === null)
            throw new Error("The parameter 'itemPerPage' cannot be null.");
        else if (itemPerPage !== undefined)
            url_ += "itemPerPage=" + encodeURIComponent("" + itemPerPage) + "&";
        if (filterByName === null)
            throw new Error("The parameter 'filterByName' cannot be null.");
        else if (filterByName !== undefined)
            url_ += "filterByName=" + encodeURIComponent("" + filterByName) + "&";
        url_ = url_.replace(/[?&]$/, "");

        let options_ = <RequestInit>{
            method: "GET",
            headers: {
                "Accept": "text/plain"
            }
        };

        return this.http.fetch(url_, options_).then((_response: Response) => {
            return this.processFilterData(_response);
        });
    }

    protected processFilterData(response: Response): Promise<PerfumeModel[]> {
        const status = response.status;
        let _headers: any = {}; if (response.headers && response.headers.forEach) { response.headers.forEach((v: any, k: any) => _headers[k] = v); };
        if (status === 200) {
            return response.text().then((_responseText) => {
                let result200: any = null;
                result200 = _responseText === "" ? null : <PerfumeModel[]>JSON.parse(_responseText, this.jsonParseReviver);
                return result200;
            });
        } else if (status !== 200 && status !== 204) {
            return response.text().then((_responseText) => {
                return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            });
        }
        return Promise.resolve<PerfumeModel[]>(<any>null);
    }

    /**
     * @return Success
     */
    totalData(): Promise<number> {
        let url_ = this.baseUrl + "/api/v1/perfume/total-data";
        url_ = url_.replace(/[?&]$/, "");

        let options_ = <RequestInit>{
            method: "GET",
            headers: {
                "Accept": "text/plain"
            }
        };

        return this.http.fetch(url_, options_).then((_response: Response) => {
            return this.processTotalData(_response);
        });
    }

    protected processTotalData(response: Response): Promise<number> {
        const status = response.status;
        let _headers: any = {}; if (response.headers && response.headers.forEach) { response.headers.forEach((v: any, k: any) => _headers[k] = v); };
        if (status === 200) {
            return response.text().then((_responseText) => {
                let result200: any = null;
                result200 = _responseText === "" ? null : <number>JSON.parse(_responseText, this.jsonParseReviver);
                return result200;
            });
        } else if (status !== 200 && status !== 204) {
            return response.text().then((_responseText) => {
                return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            });
        }
        return Promise.resolve<number>(<any>null);
    }
}

export interface PerfumeModel {
    perfumeId?: string;
    perfumeName: string;
    quantity: string;
}

export interface ResponseModel {
    responseMessage?: string | undefined;
    status?: string | undefined;
}

export class ApiException extends Error {
    message: string;
    status: number; 
    response: string; 
    headers: { [key: string]: any; };
    result: any; 

    constructor(message: string, status: number, response: string, headers: { [key: string]: any; }, result: any) {
        super();

        this.message = message;
        this.status = status;
        this.response = response;
        this.headers = headers;
        this.result = result;
    }

    protected isApiException = true;

    static isApiException(obj: any): obj is ApiException {
        return obj.isApiException === true;
    }
}

function throwException(message: string, status: number, response: string, headers: { [key: string]: any; }, result?: any): any {
    if (result !== null && result !== undefined)
        throw result;
    else
        throw new ApiException(message, status, response, headers, null);
}
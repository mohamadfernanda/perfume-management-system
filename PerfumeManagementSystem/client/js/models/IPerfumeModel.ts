﻿export interface PerfumeModelTemp {
    perfumeId?: string;
    perfumeName?: string;
    quantity?: string;
}
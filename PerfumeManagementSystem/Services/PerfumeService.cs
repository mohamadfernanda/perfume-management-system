﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PerfumeManagementSystem.Entities;
using PerfumeManagementSystem.Models;

namespace PerfumeManagementSystem.Services
{
    public class PerfumeService
    {
        private readonly ManagementDbContext _db;

        public PerfumeService(ManagementDbContext dbContext)
        {
            this._db = dbContext;
        }

        public async Task<bool> InsertPerfume(PerfumeModel perfumeModel)
        {
            this._db.Add(new Perfume
            {
                PerfumeId = Guid.NewGuid(),
                PerfumeName = perfumeModel.PerfumeName,
                Quantity = perfumeModel.Quantity,
            });

            await this._db.SaveChangesAsync();
            return true;
        }

        public int GetTotalData()
        {
            var totalPerfume = this._db
                .Perfumes
                .Count();

            return totalPerfume;
        }

        public async Task<List<PerfumeModel>> GetAllPerfumesAsync()
        {
            var perfumes = await this._db
                .Perfumes
                .Select(Q => new PerfumeModel
                {
                    PerfumeName = Q.PerfumeName,
                    PerfumeId = Q.PerfumeId,
                    Quantity = Q.Quantity,
                })
                .AsNoTracking()
                .ToListAsync();

            return perfumes;
        }

        public async Task<List<PerfumeModel>> GetAsync(int pageIndex, int itemPerPage, string filterByName)
        {
            // set employee to queryable for  filter
            var query = this._db
                .Perfumes
                .AsQueryable();

            // for filter
            if (string.IsNullOrEmpty(filterByName) == false)
            {
                query = query
                    .Where(Q => Q.PerfumeName.StartsWith(filterByName));
            }

            // get data after filtering
            var perfumes = await query
                .Select(Q => new PerfumeModel
                {
                    PerfumeId = Q.PerfumeId,
                    PerfumeName = Q.PerfumeName,
                    Quantity = Q.Quantity,
                })
                .Skip((pageIndex - 1) * itemPerPage)
                .Take(itemPerPage)
                .AsNoTracking()
                .ToListAsync();

            return perfumes;
        }

        public async Task<PerfumeModel> GetSpesificPerfumeAsync(Guid perfumeId)
        {
            var perfume = await this._db
                .Perfumes
                .Where(Q => Q.PerfumeId == perfumeId)
                .Select(Q => new PerfumeModel
                {
                    PerfumeId = Q.PerfumeId,
                    PerfumeName = Q.PerfumeName,
                    Quantity = Q.Quantity,
                })
                .FirstOrDefaultAsync();

            return perfume;
        }

        public async Task<bool> UpdatePerfumeAsync(PerfumeModel perfume)
        {
            var perfumeModel = await this._db
                .Perfumes
                .Where(Q => Q.PerfumeId == perfume.PerfumeId)
                .FirstOrDefaultAsync();

            if (perfumeModel == null)
            {
                return false;
            }

            perfumeModel.PerfumeName = perfume.PerfumeName;
            perfumeModel.Quantity = perfume.Quantity;

            await this._db.SaveChangesAsync();

            return true;
        }

        public async Task<bool> DeletePerfumeAsync(Guid perfumeId)
        {
            var perfumeModel = await this._db.Perfumes.Where(Q => Q.PerfumeId == perfumeId).FirstOrDefaultAsync();

            if (perfumeModel == null)
            {
                return false;
            }

            this._db.Remove(perfumeModel);
            await this._db.SaveChangesAsync();

            return true;
        }
    }
}
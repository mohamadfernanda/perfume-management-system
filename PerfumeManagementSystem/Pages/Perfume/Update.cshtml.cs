﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PerfumeManagementSystem.Models;
using PerfumeManagementSystem.Services;

namespace PerfumeManagementSystem.Pages.Perfume
{
    public class UpdateModel : PageModel
    {
        private readonly PerfumeService ServiceMan;

        public UpdateModel(PerfumeService perfumeService)
        {
            this.ServiceMan = perfumeService;
        }

        [BindProperty]
        public PerfumeModel CurrentPerfume { set; get; }

        public async Task OnGetAsync(Guid id)
        {
            CurrentPerfume = await ServiceMan.GetSpesificPerfumeAsync(id);
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (ModelState.IsValid == false)
            {
                return Page();
            }

            var isExist = await ServiceMan.UpdatePerfumeAsync(CurrentPerfume);

            if (isExist == false)
            {
                return Page();
            }
            return Redirect("/perfume");
         }
    }
}

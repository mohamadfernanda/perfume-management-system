﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PerfumeManagementSystem.Models;
using PerfumeManagementSystem.Services;

namespace PerfumeManagementSystem.Pages.Perfume
{
    public class CreateModel : PageModel
    {
        private readonly PerfumeService ServiceMan;

        public CreateModel(PerfumeService employeeService)
        {
            this.ServiceMan = employeeService;
        }

        [BindProperty]
        public PerfumeModel NewPerfume { set; get; }
        public void OnGet()
        {
        }

        public async Task<IActionResult> OnPost()
        {
            if (ModelState.IsValid == false)
            {
                return Page();
            }

            await ServiceMan.InsertPerfume(NewPerfume);
            return Redirect("./");
        }
    }
}

﻿using System;
namespace PerfumeManagementSystem.Models
{
    public class ResponseModel
    {
        public string ResponseMessage { set; get; }
        public string Status { set; get; }
    }
}

﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PerfumeManagementSystem.Models
{
    public class PerfumeModel
    {
        /// <summary>
        /// Property of Perfume ID
        /// </summary>
        public Guid PerfumeId { set; get; }


        /// <summary>
        /// Property of Perfume Name
        /// </summary>
        [Required]
        [StringLength(7, MinimumLength = 3)]
        [Display(Name = "Perfume Name")]
        public string PerfumeName { set; get; }


        /// <summary>
        /// Property of Perfume Quantity
        /// </summary>
        [Required]
        [Display(Name = "Quantity")]
        public int Quantity { set; get; }
    }
}

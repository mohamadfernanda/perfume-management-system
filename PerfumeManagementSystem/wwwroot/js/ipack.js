/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is not neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./client/js/icons.ts":
/*!****************************!*\
  !*** ./client/js/icons.ts ***!
  \****************************/
/*! namespace exports */
/*! exports [not provided] [no usage info] */
/*! runtime requirements: __webpack_require__, __webpack_require__.r, __webpack_exports__, __webpack_require__.* */
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _fortawesome_fontawesome_svg_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @fortawesome/fontawesome-svg-core */ \"./node_modules/@fortawesome/fontawesome-svg-core/index.es.js\");\n/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @fortawesome/free-solid-svg-icons */ \"./node_modules/@fortawesome/free-solid-svg-icons/index.es.js\");\n\n\n_fortawesome_fontawesome_svg_core__WEBPACK_IMPORTED_MODULE_0__.library.add(_fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_1__.faChevronUp, _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_1__.faCheck);\n// Reduce dll size by only importing icons which are actually being used:\n// https://fontawesome.com/how-to-use/use-with-node-js\n//# sourceMappingURL=icons.js.map//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9jbGllbnQvanMvaWNvbnMudHM/ZmMxNCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7QUFBNEQ7QUFDYTtBQUd6RSwwRUFBVyxDQUFDLDBFQUFXLEVBQUUsc0VBQU8sQ0FBQyxDQUFDO0FBRWxDLHlFQUF5RTtBQUN6RSxzREFBc0QiLCJmaWxlIjoiLi9jbGllbnQvanMvaWNvbnMudHMuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBsaWJyYXJ5IH0gZnJvbSAnQGZvcnRhd2Vzb21lL2ZvbnRhd2Vzb21lLXN2Zy1jb3JlJztcclxuaW1wb3J0IHsgZmFDaGV2cm9uVXAsIGZhQ2hlY2sgfSBmcm9tICdAZm9ydGF3ZXNvbWUvZnJlZS1zb2xpZC1zdmctaWNvbnMnO1xyXG5pbXBvcnQgeyB9IGZyb20gJ0Bmb3J0YXdlc29tZS9mcmVlLXJlZ3VsYXItc3ZnLWljb25zJztcclxuXHJcbmxpYnJhcnkuYWRkKGZhQ2hldnJvblVwLCBmYUNoZWNrKTtcclxuXHJcbi8vIFJlZHVjZSBkbGwgc2l6ZSBieSBvbmx5IGltcG9ydGluZyBpY29ucyB3aGljaCBhcmUgYWN0dWFsbHkgYmVpbmcgdXNlZDpcclxuLy8gaHR0cHM6Ly9mb250YXdlc29tZS5jb20vaG93LXRvLXVzZS91c2Utd2l0aC1ub2RlLWpzXHJcbiJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./client/js/icons.ts\n");

/***/ }),

/***/ "./client/js/index.ts":
/*!****************************!*\
  !*** ./client/js/index.ts ***!
  \****************************/
/*! namespace exports */
/*! exports [not provided] [no usage info] */
/*! runtime requirements: __webpack_require__, __webpack_require__.r, __webpack_exports__, __webpack_require__.* */
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var ts_polyfill_lib_es2016_array_include__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ts-polyfill/lib/es2016-array-include */ \"./node_modules/ts-polyfill/lib/es2016-array-include.js\");\n/* harmony import */ var ts_polyfill_lib_es2017_object__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ts-polyfill/lib/es2017-object */ \"./node_modules/ts-polyfill/lib/es2017-object.js\");\n/* harmony import */ var ts_polyfill_lib_es2017_string__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ts-polyfill/lib/es2017-string */ \"./node_modules/ts-polyfill/lib/es2017-string.js\");\n/* harmony import */ var ts_polyfill_lib_es2018_async_iterable__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ts-polyfill/lib/es2018-async-iterable */ \"./node_modules/ts-polyfill/lib/es2018-async-iterable.js\");\n/* harmony import */ var ts_polyfill_lib_es2018_promise__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ts-polyfill/lib/es2018-promise */ \"./node_modules/ts-polyfill/lib/es2018-promise.js\");\n/* harmony import */ var ts_polyfill_lib_es2019_array__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ts-polyfill/lib/es2019-array */ \"./node_modules/ts-polyfill/lib/es2019-array.js\");\n/* harmony import */ var ts_polyfill_lib_es2019_object__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ts-polyfill/lib/es2019-object */ \"./node_modules/ts-polyfill/lib/es2019-object.js\");\n/* harmony import */ var ts_polyfill_lib_es2019_string__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ts-polyfill/lib/es2019-string */ \"./node_modules/ts-polyfill/lib/es2019-string.js\");\n/* harmony import */ var ts_polyfill_lib_es2020_string__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ts-polyfill/lib/es2020-string */ \"./node_modules/ts-polyfill/lib/es2020-string.js\");\n/* harmony import */ var _icons__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./icons */ \"./client/js/icons.ts\");\n/* harmony import */ var _vue_project__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./vue-project */ \"./client/js/vue-project.ts\");\n\n\n\n // for-await-of\n\n\n\n\n\n\n\n//# sourceMappingURL=index.js.map//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9jbGllbnQvanMvaW5kZXgudHM/ODBlMSJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7QUFBOEM7QUFDUDtBQUNBO0FBQ1EsQ0FBRyxlQUFlO0FBQ3pCO0FBQ0Y7QUFDQztBQUNBO0FBQ0E7QUFFdEI7QUFDTSIsImZpbGUiOiIuL2NsaWVudC9qcy9pbmRleC50cy5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAndHMtcG9seWZpbGwvbGliL2VzMjAxNi1hcnJheS1pbmNsdWRlJztcclxuaW1wb3J0ICd0cy1wb2x5ZmlsbC9saWIvZXMyMDE3LW9iamVjdCc7XHJcbmltcG9ydCAndHMtcG9seWZpbGwvbGliL2VzMjAxNy1zdHJpbmcnO1xyXG5pbXBvcnQgJ3RzLXBvbHlmaWxsL2xpYi9lczIwMTgtYXN5bmMtaXRlcmFibGUnOyAgIC8vIGZvci1hd2FpdC1vZlxyXG5pbXBvcnQgJ3RzLXBvbHlmaWxsL2xpYi9lczIwMTgtcHJvbWlzZSc7XHJcbmltcG9ydCAndHMtcG9seWZpbGwvbGliL2VzMjAxOS1hcnJheSc7XHJcbmltcG9ydCAndHMtcG9seWZpbGwvbGliL2VzMjAxOS1vYmplY3QnO1xyXG5pbXBvcnQgJ3RzLXBvbHlmaWxsL2xpYi9lczIwMTktc3RyaW5nJztcclxuaW1wb3J0ICd0cy1wb2x5ZmlsbC9saWIvZXMyMDIwLXN0cmluZyc7XHJcblxyXG5pbXBvcnQgJy4vaWNvbnMnO1xyXG5pbXBvcnQgJy4vdnVlLXByb2plY3QnO1xyXG4iXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./client/js/index.ts\n");

/***/ }),

/***/ "./client/js/vue-project.ts":
/*!**********************************!*\
  !*** ./client/js/vue-project.ts ***!
  \**********************************/
/*! namespace exports */
/*! exports [not provided] [no usage info] */
/*! runtime requirements: __webpack_require__, __webpack_require__.r, __webpack_exports__, __webpack_require__.e, __webpack_require__.t, __webpack_require__.* */
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ \"./node_modules/vue/dist/vue.runtime.esm.js\");\n/* harmony import */ var _fortawesome_vue_fontawesome__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @fortawesome/vue-fontawesome */ \"./node_modules/@fortawesome/vue-fontawesome/index.es.js\");\n\n\n// register child components usable inside Top-Level Components:\n// https://vuejs.org/v2/guide/components-registration.html#Global-Registration\n// https://vuejs.org/v2/guide/components-registration.html#Local-Registration\nvue__WEBPACK_IMPORTED_MODULE_0__.default.component('fa-icon', _fortawesome_vue_fontawesome__WEBPACK_IMPORTED_MODULE_1__.FontAwesomeIcon);\n// https://vuejs.org/v2/guide/components-dynamic-async.html#Async-Components\nvue__WEBPACK_IMPORTED_MODULE_0__.default.component('validation-provider', () => __webpack_require__.e(/*! import() */ \"vendors-node_modules_vee-validate_dist_vee-validate_full_js\").then(__webpack_require__.t.bind(__webpack_require__, /*! vee-validate */ \"./node_modules/vee-validate/dist/vee-validate.full.js\", 7)).then(esm => esm.ValidationProvider));\nvue__WEBPACK_IMPORTED_MODULE_0__.default.component('validation-observer', () => __webpack_require__.e(/*! import() */ \"vendors-node_modules_vee-validate_dist_vee-validate_full_js\").then(__webpack_require__.t.bind(__webpack_require__, /*! vee-validate */ \"./node_modules/vee-validate/dist/vee-validate.full.js\", 7)).then(esm => esm.ValidationObserver));\n/**\n * For each matching HTML Elements, render and mount a Vue Async Component without props.\n * @param selector HTML Element selector query\n * @param lazyComponent Vue Async Component\n */\nfunction renderAsyncComponent(selector, lazyComponent) {\n    for (const el of document.querySelectorAll(selector)) {\n        new vue__WEBPACK_IMPORTED_MODULE_0__.default({\n            el: el,\n            render: function (h) {\n                return h(lazyComponent);\n            }\n        });\n    }\n}\nrenderAsyncComponent('hello-world', () => Promise.all(/*! import() */[__webpack_require__.e(\"vendors-node_modules_tslib_tslib_es6_js-node_modules_vue-class-component_dist_vue-class-compo-8113e4\"), __webpack_require__.e(\"client_js_components_HelloWorld_vue\")]).then(__webpack_require__.bind(__webpack_require__, /*! ./components/HelloWorld.vue */ \"./client/js/components/HelloWorld.vue\")));\n// now <hello-world></hello-world> can be invoked in DOM!\n// add more components to be rendered in DOM here ^\n//# sourceMappingURL=vue-project.js.map//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9jbGllbnQvanMvdnVlLXByb2plY3QudHM/NzI1NyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7QUFBaUM7QUFDOEI7QUFFL0QsZ0VBQWdFO0FBQ2hFLDhFQUE4RTtBQUM5RSw2RUFBNkU7QUFDN0Usa0RBQWEsQ0FBQyxTQUFTLEVBQUUseUVBQWUsQ0FBQyxDQUFDO0FBQzFDLDRFQUE0RTtBQUM1RSxrREFBYSxDQUFDLHFCQUFxQixFQUFFLEdBQUcsRUFBRSxDQUFDLDBPQUFzQixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLENBQUM7QUFDdkcsa0RBQWEsQ0FBQyxxQkFBcUIsRUFBRSxHQUFHLEVBQUUsQ0FBQywwT0FBc0IsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDO0FBT3ZHOzs7O0dBSUc7QUFDSCxTQUFTLG9CQUFvQixDQUFDLFFBQWdCLEVBQUUsYUFBZ0M7SUFDNUUsS0FBSyxNQUFNLEVBQUUsSUFBSSxRQUFRLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLEVBQUU7UUFDbEQsSUFBSSx3Q0FBRyxDQUFDO1lBQ0osRUFBRSxFQUFFLEVBQUU7WUFDTixNQUFNLEVBQUUsVUFBVSxDQUFDO2dCQUNmLE9BQU8sQ0FBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDO1lBQzVCLENBQUM7U0FDSixDQUFDLENBQUM7S0FDTjtBQUNMLENBQUM7QUFFRCxvQkFBb0IsQ0FBQyxhQUFhLEVBQUUsR0FBRyxFQUFFLENBQUMseVZBQXFDLENBQUMsQ0FBQztBQUNqRix5REFBeUQ7QUFDekQsbURBQW1EIiwiZmlsZSI6Ii4vY2xpZW50L2pzL3Z1ZS1wcm9qZWN0LnRzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFZ1ZSwgeyBWTm9kZSB9IGZyb20gJ3Z1ZSc7XHJcbmltcG9ydCB7IEZvbnRBd2Vzb21lSWNvbiB9IGZyb20gJ0Bmb3J0YXdlc29tZS92dWUtZm9udGF3ZXNvbWUnO1xyXG5cclxuLy8gcmVnaXN0ZXIgY2hpbGQgY29tcG9uZW50cyB1c2FibGUgaW5zaWRlIFRvcC1MZXZlbCBDb21wb25lbnRzOlxyXG4vLyBodHRwczovL3Z1ZWpzLm9yZy92Mi9ndWlkZS9jb21wb25lbnRzLXJlZ2lzdHJhdGlvbi5odG1sI0dsb2JhbC1SZWdpc3RyYXRpb25cclxuLy8gaHR0cHM6Ly92dWVqcy5vcmcvdjIvZ3VpZGUvY29tcG9uZW50cy1yZWdpc3RyYXRpb24uaHRtbCNMb2NhbC1SZWdpc3RyYXRpb25cclxuVnVlLmNvbXBvbmVudCgnZmEtaWNvbicsIEZvbnRBd2Vzb21lSWNvbik7XHJcbi8vIGh0dHBzOi8vdnVlanMub3JnL3YyL2d1aWRlL2NvbXBvbmVudHMtZHluYW1pYy1hc3luYy5odG1sI0FzeW5jLUNvbXBvbmVudHNcclxuVnVlLmNvbXBvbmVudCgndmFsaWRhdGlvbi1wcm92aWRlcicsICgpID0+IGltcG9ydCgndmVlLXZhbGlkYXRlJykudGhlbihlc20gPT4gZXNtLlZhbGlkYXRpb25Qcm92aWRlcikpO1xyXG5WdWUuY29tcG9uZW50KCd2YWxpZGF0aW9uLW9ic2VydmVyJywgKCkgPT4gaW1wb3J0KCd2ZWUtdmFsaWRhdGUnKS50aGVuKGVzbSA9PiBlc20uVmFsaWRhdGlvbk9ic2VydmVyKSk7XHJcblxyXG4vKipcclxuICogQSBmYWN0b3J5IGZ1bmN0aW9uIHJldHVybmluZyBhIFByb21pc2Ugb2YgVnVlIFNpbmdsZS1GaWxlIENvbXBvbmVudC5cclxuICovXHJcbnR5cGUgVnVlQXN5bmNDb21wb25lbnQgPSAoKSA9PiBQcm9taXNlPHR5cGVvZiBpbXBvcnQoJyoudnVlJyk+O1xyXG5cclxuLyoqXHJcbiAqIEZvciBlYWNoIG1hdGNoaW5nIEhUTUwgRWxlbWVudHMsIHJlbmRlciBhbmQgbW91bnQgYSBWdWUgQXN5bmMgQ29tcG9uZW50IHdpdGhvdXQgcHJvcHMuXHJcbiAqIEBwYXJhbSBzZWxlY3RvciBIVE1MIEVsZW1lbnQgc2VsZWN0b3IgcXVlcnlcclxuICogQHBhcmFtIGxhenlDb21wb25lbnQgVnVlIEFzeW5jIENvbXBvbmVudFxyXG4gKi9cclxuZnVuY3Rpb24gcmVuZGVyQXN5bmNDb21wb25lbnQoc2VsZWN0b3I6IHN0cmluZywgbGF6eUNvbXBvbmVudDogVnVlQXN5bmNDb21wb25lbnQpOiB2b2lkIHtcclxuICAgIGZvciAoY29uc3QgZWwgb2YgZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChzZWxlY3RvcikpIHtcclxuICAgICAgICBuZXcgVnVlKHtcclxuICAgICAgICAgICAgZWw6IGVsLFxyXG4gICAgICAgICAgICByZW5kZXI6IGZ1bmN0aW9uIChoKTogVk5vZGUge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGgobGF6eUNvbXBvbmVudCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxufVxyXG5cclxucmVuZGVyQXN5bmNDb21wb25lbnQoJ2hlbGxvLXdvcmxkJywgKCkgPT4gaW1wb3J0KCcuL2NvbXBvbmVudHMvSGVsbG9Xb3JsZC52dWUnKSk7XHJcbi8vIG5vdyA8aGVsbG8td29ybGQ+PC9oZWxsby13b3JsZD4gY2FuIGJlIGludm9rZWQgaW4gRE9NIVxyXG4vLyBhZGQgbW9yZSBjb21wb25lbnRzIHRvIGJlIHJlbmRlcmVkIGluIERPTSBoZXJlIF5cclxuIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./client/js/vue-project.ts\n");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		if(__webpack_module_cache__[moduleId]) {
/******/ 			return __webpack_module_cache__[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = __webpack_modules__;
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/create fake namespace object */
/******/ 	(() => {
/******/ 		// create a fake namespace object
/******/ 		// mode & 1: value is a module id, require it
/******/ 		// mode & 2: merge all properties of value into the ns
/******/ 		// mode & 4: return value when already ns object
/******/ 		// mode & 8|1: behave like require
/******/ 		__webpack_require__.t = function(value, mode) {
/******/ 			if(mode & 1) value = this(value);
/******/ 			if(mode & 8) return value;
/******/ 			if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 			var ns = Object.create(null);
/******/ 			__webpack_require__.r(ns);
/******/ 			var def = {};
/******/ 			if(mode & 2 && typeof value == 'object' && value) {
/******/ 				for(const key in value) def[key] = () => value[key];
/******/ 			}
/******/ 			def['default'] = () => value;
/******/ 			__webpack_require__.d(ns, def);
/******/ 			return ns;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/ensure chunk */
/******/ 	(() => {
/******/ 		__webpack_require__.f = {};
/******/ 		// This file contains only the entry chunk.
/******/ 		// The chunk loading function for additional chunks
/******/ 		__webpack_require__.e = (chunkId) => {
/******/ 			return Promise.all(Object.keys(__webpack_require__.f).reduce((promises, key) => {
/******/ 				__webpack_require__.f[key](chunkId, promises);
/******/ 				return promises;
/******/ 			}, []));
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/get javascript chunk filename */
/******/ 	(() => {
/******/ 		// This function allow to reference async chunks
/******/ 		__webpack_require__.u = (chunkId) => {
/******/ 			// return url for filenames based on template
/******/ 			return "ipack." + chunkId + ".js";
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/global */
/******/ 	(() => {
/******/ 		__webpack_require__.g = (function() {
/******/ 			if (typeof globalThis === 'object') return globalThis;
/******/ 			try {
/******/ 				return this || new Function('return this')();
/******/ 			} catch (e) {
/******/ 				if (typeof window === 'object') return window;
/******/ 			}
/******/ 		})();
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => Object.prototype.hasOwnProperty.call(obj, prop)
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/publicPath */
/******/ 	(() => {
/******/ 		__webpack_require__.p = "js/";
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/jsonp chunk loading */
/******/ 	(() => {
/******/ 		// object to store loaded and loading chunks
/******/ 		// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 		// Promise = chunk loading, 0 = chunk loaded
/******/ 		var installedChunks = {
/******/ 			"main": 0
/******/ 		};
/******/ 		
/******/ 		var deferredModules = [
/******/ 			["./client/js/index.ts","dll"]
/******/ 		];
/******/ 		__webpack_require__.f.j = (chunkId, promises) => {
/******/ 				// JSONP chunk loading for javascript
/******/ 				var installedChunkData = __webpack_require__.o(installedChunks, chunkId) ? installedChunks[chunkId] : undefined;
/******/ 				if(installedChunkData !== 0) { // 0 means "already installed".
/******/ 		
/******/ 					// a Promise means "currently loading".
/******/ 					if(installedChunkData) {
/******/ 						promises.push(installedChunkData[2]);
/******/ 					} else {
/******/ 						if(true) { // all chunks have JS
/******/ 							// setup Promise in chunk cache
/******/ 							var promise = new Promise((resolve, reject) => {
/******/ 								installedChunkData = installedChunks[chunkId] = [resolve, reject];
/******/ 							});
/******/ 							promises.push(installedChunkData[2] = promise);
/******/ 		
/******/ 							// start chunk loading
/******/ 							var url = __webpack_require__.p + __webpack_require__.u(chunkId);
/******/ 							var loadingEnded = () => {
/******/ 								if(__webpack_require__.o(installedChunks, chunkId)) {
/******/ 									installedChunkData = installedChunks[chunkId];
/******/ 									if(installedChunkData !== 0) installedChunks[chunkId] = undefined;
/******/ 									if(installedChunkData) return installedChunkData[1];
/******/ 								}
/******/ 							};
/******/ 							var script = document.createElement('script');
/******/ 							var onScriptComplete;
/******/ 		
/******/ 							script.charset = 'utf-8';
/******/ 							script.timeout = 120;
/******/ 							if (__webpack_require__.nc) {
/******/ 								script.setAttribute("nonce", __webpack_require__.nc);
/******/ 							}
/******/ 							script.src = url;
/******/ 		
/******/ 							// create error before stack unwound to get useful stacktrace later
/******/ 							var error = new Error();
/******/ 							onScriptComplete = (event) => {
/******/ 								onScriptComplete = () => {
/******/ 		
/******/ 								}
/******/ 								// avoid mem leaks in IE.
/******/ 								script.onerror = script.onload = null;
/******/ 								clearTimeout(timeout);
/******/ 								var reportError = loadingEnded();
/******/ 								if(reportError) {
/******/ 									var errorType = event && (event.type === 'load' ? 'missing' : event.type);
/******/ 									var realSrc = event && event.target && event.target.src;
/******/ 									error.message = 'Loading chunk ' + chunkId + ' failed.\n(' + errorType + ': ' + realSrc + ')';
/******/ 									error.name = 'ChunkLoadError';
/******/ 									error.type = errorType;
/******/ 									error.request = realSrc;
/******/ 									reportError(error);
/******/ 								}
/******/ 							}
/******/ 							;
/******/ 							var timeout = setTimeout(() => {
/******/ 								onScriptComplete({ type: 'timeout', target: script })
/******/ 							}, 120000);
/******/ 							script.onerror = script.onload = onScriptComplete;
/******/ 							document.head.appendChild(script);
/******/ 						} else installedChunks[chunkId] = 0;
/******/ 		
/******/ 						// no HMR
/******/ 					}
/******/ 				}
/******/ 		};
/******/ 		
/******/ 		// no prefetching
/******/ 		
/******/ 		// no preloaded
/******/ 		
/******/ 		// no HMR
/******/ 		
/******/ 		// no HMR manifest
/******/ 		
/******/ 		var checkDeferredModules = () => {
/******/ 		
/******/ 		};
/******/ 		function checkDeferredModulesImpl() {
/******/ 			var result;
/******/ 			for(var i = 0; i < deferredModules.length; i++) {
/******/ 				var deferredModule = deferredModules[i];
/******/ 				var fulfilled = true;
/******/ 				for(var j = 1; j < deferredModule.length; j++) {
/******/ 					var depId = deferredModule[j];
/******/ 					if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 				}
/******/ 				if(fulfilled) {
/******/ 					deferredModules.splice(i--, 1);
/******/ 					result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 				}
/******/ 			}
/******/ 			if(deferredModules.length === 0) {
/******/ 				__webpack_require__.x();
/******/ 				__webpack_require__.x = () => {
/******/ 		
/******/ 				}
/******/ 			}
/******/ 			return result;
/******/ 		}
/******/ 		__webpack_require__.x = () => {
/******/ 			// reset startup function so it can be called again when more startup code is added
/******/ 			__webpack_require__.x = () => {
/******/ 		
/******/ 			}
/******/ 			jsonpArray = jsonpArray.slice();
/******/ 			for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 			return (checkDeferredModules = checkDeferredModulesImpl)();
/******/ 		};
/******/ 		
/******/ 		// install a JSONP callback for chunk loading
/******/ 		function webpackJsonpCallback(data) {
/******/ 			var chunkIds = data[0];
/******/ 			var moreModules = data[1];
/******/ 			var executeModules = data[2];
/******/ 			var runtime = data[3];
/******/ 			// add "moreModules" to the modules object,
/******/ 			// then flag all "chunkIds" as loaded and fire callback
/******/ 			var moduleId, chunkId, i = 0, resolves = [];
/******/ 			for(;i < chunkIds.length; i++) {
/******/ 				chunkId = chunkIds[i];
/******/ 				if(__webpack_require__.o(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 					resolves.push(installedChunks[chunkId][0]);
/******/ 				}
/******/ 				installedChunks[chunkId] = 0;
/******/ 			}
/******/ 			for(moduleId in moreModules) {
/******/ 				if(__webpack_require__.o(moreModules, moduleId)) {
/******/ 					__webpack_require__.m[moduleId] = moreModules[moduleId];
/******/ 				}
/******/ 			}
/******/ 			if(runtime) runtime(__webpack_require__);
/******/ 			if(parentJsonpFunction) parentJsonpFunction(data);
/******/ 			while(resolves.length) {
/******/ 				resolves.shift()();
/******/ 			}
/******/ 		
/******/ 			// add entry modules from loaded chunk to deferred list
/******/ 			if(executeModules) deferredModules.push.apply(deferredModules, executeModules);
/******/ 		
/******/ 			// run deferred modules when all chunks ready
/******/ 			return checkDeferredModules();
/******/ 		};
/******/ 		
/******/ 		var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 		var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 		jsonpArray.push = webpackJsonpCallback;
/******/ 		var parentJsonpFunction = oldJsonpFunction;
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	// run startup
/******/ 	return __webpack_require__.x();
/******/ })()
;